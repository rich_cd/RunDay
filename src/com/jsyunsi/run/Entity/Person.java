/**
 * 
 */
package com.jsyunsi.run.Entity;

import java.awt.Graphics;
import java.awt.Paint;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * @ 陈东
 * @ 2018年7月13日
 * @ 上午10:29:24
 * @ 日常搬砖
 * 角色实体类，包含角色
 */
public class Person {
	//坐标
	private int x,y;
	//得分
	private int score;
	//图片宽高
	public static final int WIDTH = 90;
	public static final int HEIGHT = 100;

	
	//玩家图片数组
	private BufferedImage[] images;		//private只能在当期类和子类中使用，防止其他类中命名相同出错
	private static BufferedImage rw1,rw2,rw3,rw4,rw5,rw6,rw7,rw8,rw9,rw10,rw11,rw12,rw13;
	//宠物图片数组
	private BufferedImage[] pets;
	private static BufferedImage p1,p2,p3,p4,p5,p6;
	//当前显示图片
	private BufferedImage image;
	private BufferedImage pet;
	//生命值
	private int life = 10;
	//距离
	int distanceIndex;
	//切换图片的变量
	private int index;
	private int index2;
	
	//构造方法
	public Person() {
		x = 50;
		y = 315;
		life = 10;
		score =0;
		initPerson();
		initPet();
		images = new BufferedImage[] {rw1,rw2,rw3,rw4,rw5,rw6,rw7,rw8,rw9,rw10,rw11,rw12,rw13};
		pets = new BufferedImage[] {p1,p2,p3,p4,p5,p6};
	}
	
	//移动的方法
	public void step() {
		//玩家图片切换
		image = images[index++/6%images.length];
		//宠物图片切换
		pet = pets[index2++/6%pets.length];
		drop();
	}
	//绘制的方法
	public void paintPerson(Graphics g) {
		g.drawImage(image, x, y, WIDTH,HEIGHT,null);
		g.drawImage(pet, x+145, 351, 67,72,null);
	}
	//玩家自由下落的方法
	public void drop() {
		y+=1;
		if(y>=315) {
			y=315;
		}
	}
	
	private void initPerson() {
		try {
			rw1 = ImageIO.read(new File("image/1.png"));
			rw2 = ImageIO.read(new File("image/2.png"));
			rw3 = ImageIO.read(new File("image/3.png"));
			rw4 = ImageIO.read(new File("image/4.png"));
			rw5 = ImageIO.read(new File("image/5.png"));
			rw6 = ImageIO.read(new File("image/6.png"));
			rw7 = ImageIO.read(new File("image/7.png"));
			rw8 = ImageIO.read(new File("image/8.png"));
			rw9 = ImageIO.read(new File("image/9.png"));
			rw10 = ImageIO.read(new File("image/f10.png"));
			rw11 = ImageIO.read(new File("image/f11.png"));
			rw12 = ImageIO.read(new File("image/f12.png"));
			rw13 = ImageIO.read(new File("image/f13.png"));
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}
	
	private void initPet() {
		try {
			p1 = ImageIO.read(new File("image/d1.png"));
			p2 = ImageIO.read(new File("image/d2.png"));
			p3 = ImageIO.read(new File("image/d3.png"));
			p4 = ImageIO.read(new File("image/d4.png"));
			p5 = ImageIO.read(new File("image/d5.png"));
			p6 = ImageIO.read(new File("image/d6.png"));
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	}

	//右键-源码-生成getter和setter
	/**
	 * @return x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return distanceIndex
	 */
	public int getDistanceIndex() {
		return distanceIndex;
	}

	/**
	 * @param distanceIndex 要设置的 distanceIndex
	 */
	public void setDistanceIndex(int distanceIndex) {
		this.distanceIndex = distanceIndex;
	}

	/**
	 * @param x 要设置的 x
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y 要设置的 y
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * @return score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @param score 要设置的 score
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * @return images
	 */
	public BufferedImage[] getImages() {
		return images;
	}

	/**
	 * @param images 要设置的 images
	 */
	public void setImages(BufferedImage[] images) {
		this.images = images;
	}

	/**
	 * @return rw1
	 */
	public static BufferedImage getRw1() {
		return rw1;
	}

	/**
	 * @param rw1 要设置的 rw1
	 */
	public static void setRw1(BufferedImage rw1) {
		Person.rw1 = rw1;
	}

	/**
	 * @return rw2
	 */
	public static BufferedImage getRw2() {
		return rw2;
	}

	/**
	 * @param rw2 要设置的 rw2
	 */
	public static void setRw2(BufferedImage rw2) {
		Person.rw2 = rw2;
	}

	/**
	 * @return rw3
	 */
	public static BufferedImage getRw3() {
		return rw3;
	}

	/**
	 * @param rw3 要设置的 rw3
	 */
	public static void setRw3(BufferedImage rw3) {
		Person.rw3 = rw3;
	}

	/**
	 * @return rw4
	 */
	public static BufferedImage getRw4() {
		return rw4;
	}

	/**
	 * @param rw4 要设置的 rw4
	 */
	public static void setRw4(BufferedImage rw4) {
		Person.rw4 = rw4;
	}

	/**
	 * @return rw5
	 */
	public static BufferedImage getRw5() {
		return rw5;
	}

	/**
	 * @param rw5 要设置的 rw5
	 */
	public static void setRw5(BufferedImage rw5) {
		Person.rw5 = rw5;
	}

	/**
	 * @return rw6
	 */
	public static BufferedImage getRw6() {
		return rw6;
	}

	/**
	 * @param rw6 要设置的 rw6
	 */
	public static void setRw6(BufferedImage rw6) {
		Person.rw6 = rw6;
	}

	/**
	 * @return rw7
	 */
	public static BufferedImage getRw7() {
		return rw7;
	}

	/**
	 * @param rw7 要设置的 rw7
	 */
	public static void setRw7(BufferedImage rw7) {
		Person.rw7 = rw7;
	}

	/**
	 * @return rw8
	 */
	public static BufferedImage getRw8() {
		return rw8;
	}

	/**
	 * @param rw8 要设置的 rw8
	 */
	public static void setRw8(BufferedImage rw8) {
		Person.rw8 = rw8;
	}

	/**
	 * @return rw9
	 */
	public static BufferedImage getRw9() {
		return rw9;
	}

	/**
	 * @param rw9 要设置的 rw9
	 */
	public static void setRw9(BufferedImage rw9) {
		Person.rw9 = rw9;
	}

	/**
	 * @return image
	 */
	public BufferedImage getImage() {
		return image;
	}

	/**
	 * @param image 要设置的 image
	 */
	public void setImage(BufferedImage image) {
		this.image = image;
	}

	/**
	 * @return life
	 */
	public int getLife() {
		return life;
	}

	
	/**
	 * @param life 要设置的 life
	 */
	public void setLife(int life) {
		this.life = life;
	}

	/**
	 * @return index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index 要设置的 index
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @return width
	 */
	public static int getWidth() {
		return WIDTH;
	}

	/**
	 * @return heihht
	 */
	public static int getHeihht() {
		return HEIGHT;
	}
	
	
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	