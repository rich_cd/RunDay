/**
 * 
 */
package com.jsyunsi.run.JFrame;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @ 陈东
 * @ 2018年7月11日
 * @ 下午2:10:54
 * @ 日常搬砖
 */
public class MenuFrame extends JFrame implements MouseListener{
	
	JLabel start;
	JLabel help;
	JLabel exit;
	
	//加载音乐
		File file;
		URL url;
		URI uri;
		AudioClip  bgm;

	public MenuFrame() {
		
		try {
			file = new File("sound/YQ.wav");
			uri = file.toURI();
			url = uri.toURL();
			bgm = Applet.newAudioClip(url);
			bgm.loop();
		} catch (MalformedURLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		
		
		//开始按钮
		start = new JLabel(new ImageIcon("image/hh1.png"));
		start.setBounds(300, 250, 150, 40);
		//设置控件不可用
		start.setEnabled(false);
		start.addMouseListener(this);
		this.add(start);
		//帮助按钮
		help = new JLabel(new ImageIcon("image/hh2.png"));
		help.setBounds(300, 330, 150, 40);
		help.setEnabled(false);
		help.addMouseListener(this);
		this.add(help);
		//退出按钮
		exit = new JLabel(new ImageIcon("image/hh3.png"));
		exit.setBounds(300,410, 150, 40);
		exit.setEnabled(false);
		exit.addMouseListener(this);
		this.add(exit);

		//设置框架属性
		BackImage b = new BackImage();
		this.setSize(1000,550);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setUndecorated(true);
		this.setIconImage(new ImageIcon("image/115.png").getImage());
		this.add(b);
		this.setVisible(true);
	}
	
	
	public static void main(String[] args) {
		new MenuFrame();

	}
	
	class BackImage extends JPanel{
		Image bg;
		public BackImage() {
			try {
				bg = ImageIO.read(new File("image/main.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		public void paint(Graphics g) {
			super.paint(g);		
			g.drawImage(bg, 0, 0, 1000,550,null);
		}
	}
	
	class Help extends JFrame{
		Image help;
		public Help() {
			try {
				help = ImageIO.read(new File("image/bzbg.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.setSize(756,444);
			this.setIconImage(new ImageIcon("image/115.png").getImage());
			this.setLocationRelativeTo(null);		//居中
			this.setResizable(false);  				//不允许改变窗口尺寸
			this.setVisible(true);
		}
		public void paint(Graphics g) {
			super.paint(g);		
			g.drawImage(help, 0, 0,756,444,null);
		}
	}
	
	//点击鼠标执行的方法
	public void mouseClicked(MouseEvent e) {
		if(e.getSource().equals(start)) {
			new WindowFrame().Start();
			dispose();
			bgm.stop();
		}
		else if(e.getSource().equals(help)) {
			//打开帮助界面
			new Help();
		}
		else if(e.getSource().equals(exit)) {
			bgm.stop();
			System.exit(0);
		}
		
	}


	//鼠标进入
	public void mouseEntered(MouseEvent e) {
		if(e.getSource().equals(start)) {
			start.setEnabled(true);
		}
		else if(e.getSource().equals(help)) {
			help.setEnabled(true);
		}
		else if(e.getSource().equals(exit)) {
			exit.setEnabled(true);
		}
	}


	//鼠标移出
	public void mouseExited(MouseEvent e) {
		if(e.getSource().equals(start)) {
			start.setEnabled(false);
		}
		else if(e.getSource().equals(help)) {
			help.setEnabled(false);
		}
		else if(e.getSource().equals(exit)) {
			exit.setEnabled(false);
		}
	}


	
	public void mousePressed(MouseEvent arg0) {
		// TODO 自动生成的方法存根
		
	}


	/* （非 Javadoc）
	 * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO 自动生成的方法存根
		
	}

}
